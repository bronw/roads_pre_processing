

# ---------------------------------------------------------------------------
# Calculate lengths and regions to create table for percentages done by region and MU
# ---------------------------------------------------------------------------

# Import arcpy module
import arcpy
arcpy.env.overwriteOutput = True

# Script arguments
arcpy.env.workspace = arcpy.GetParameterAsText(0)#workspace



# loaded when script runs

    # Access = arcpy.GetParameterAsText(1)#Input Feature Class
    # Protected_Areas = arcpy.GetParameterAsText(2)#Input Feature Layer
    # Other_Lands = arcpy.GetParameterAsText(3)#Input Feature Layer
    # WorkUnit_Boundaries = arcpy.GetParameterAsText(4)#Input Feature Layer
    # UTM_Zones = arcpy.GetParameterAsText(5)#Input Feature Class
    # Out_Path = arcpy.GetParameterAsText(6)#Output .csv File Path
    # Out_Name = arcpy.GetParameterAsText(7)#Output .csv File Name


# default values

# Set Workspace (intermediary files will be created here)
Workspace=None

# data source locations
sde_1 = 'Database Connections\bneqpwgis02 - qpws_data - qpws.sde'
sde_2 = 'Database Connections\Connection to bneqpwdbs08.sde'
spatal_services_road_project = '\\bneqpwsmb01\spatialservices\Production\Roads_Valuation_Project'

Access = sde_1 + '\qpws_data.qpws.Features\qpws_data.qpws.Access'
Protected_Areas = sde_2 + '\qpws_prod.qpws.Administrative_and_Political_Boundaries\qpws_prod.qpws.Estate'
Other_Lands = sde_2 + '\qpws_prod.qpws.Administrative_and_Political_Boundaries\qpws_prod.qpws.Otherlands'
WorkUnit_Boundaries = spatal_services_road_project + '\PostProcessing\RVP_Files.gdb\work_unit_161118'
UTM_Zones = spatal_services_road_project + '\PostProcessing\RVP_Files.gdb\UTM_Zones_GDA94'
Out_Path = 'C:\Downloads'

#  Specify the name of the output .CSV file. Make sure you put .CSV at the end of the file name.
Out_Name = 'some_file_name.csv'  # Output .csv File Name


# Local variables:
Estate_Field_List = ['OBJECTID', 'Shape', 'LOT', 'PLAN', 'LOTPLAN',
                     'SYSINTCODE', 'ESTATENAME', 'NAME_CAPS', 'EST_TYPE', 'LEGISLATED',
                     'GAZ_AREAHA', 'ZONE', 'IUCN_CODE', 'GLR_NUMBER', 'QPWS_REG', 'CURGAZDATE',
                     'SHIRE', 'FEAT_NAME_', 'PA_AREAHA', 'ENCUMBERAN', 'IJMA', 'LONGITUDE',
                     'LATITUDE', 'LOT_AREA', 'TENURE', 'PRC', 'PARISH', 'COUNTY', 'LAC', 'SHIRE_NAME',
                     'FEAT_NAME', 'LOC', 'LOCALITY', 'CA_AREA_SQ', 'GAZAREA_HA', 'RESTYPE',
                     'Shape_Length', 'Shape_Area']

RVP_Field_List = ["OBJECTID","Shape","subcat","assettype","assetgroup","material","name","maintain",
                  "owner","status","warning","gisid","commonid","source","src_comment","vetting","retain",
                  "comments","wheelchair","fireline","access","gw","ped","horse","tb","mtb","created_user",
                  "created_date","last_edited_user","last_edited_date","visitor_id","alt_name","matrix_id",
                  "acc_guid","stereotype","road_type","surf_type","pave_type","seal_type","form_wid",
                  "carr_wid","pave_wid","seal_wid","veg_wid","form_prodr","drivabil","pave_mtdur",
                  "pave_depth","sur_txtdef","bind_age","seal_crack","seal_patch","seal_surface","RD_Len",
                  "RVP_ID","IsPlantation","FID_Protected_Areas_and_Other_Lands","nameabbrev","est_tenure",
                  "Environmen","gridcode","RA_SCR","region","Shape_Length","work_unit","mgmt_unit"]


MGA54=arcpy.SpatialReference(28354)
MGA55=arcpy.SpatialReference(28355)
MGA56=arcpy.SpatialReference(28356)

arcpy.AddMessage("Merging Layers")
arcpy.Merge_management([Protected_Areas, Other_Lands], "Protected_Areas_and_Other_Lands")

#Save Feature Class
arcpy.CopyFeatures_management(Access, "Access_Output")

# Process: Selects roads and save as feature class
arcpy.MakeFeatureLayer_management("Access_Output", "Access_Stereotype",  where_clause="subcat = 21")

# Process: Select Formed, Natural
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 1 AND material = 3")

# Process: Calculate Stereotype A
arcpy.AddMessage("Calculating Stereotype A")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"A\"", "VB", "")


# Process: Select Formed, Paved not corduroy
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 1 AND material = 2 AND pave_type <> 'D'")
# Process: Calculate Stereotype B
arcpy.AddMessage("Calculating Stereotype B")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"B\"", "VB", "")

arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 1 AND material = 2 AND pave_type IS NULL")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"B\"", "VB", "")

# Process: Select Formed, Sealed FFP
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 1 AND material = 1")
# Process: Calculate Stereotype C
arcpy.AddMessage("Calculating Stereotype C")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"C\"", "VB", "")

# Process: Select Formed, Sealed FDS
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 1 AND material = 5")
# Process: Calculate Stereotype D
arcpy.AddMessage("Calculating Stereotype D")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"D\"", "VB", "")

# Process: Select Formed, Paved Corduroy
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 1 AND material = 2 AND pave_type = 'D'")
# Process: Calculate Stereotype V
arcpy.AddMessage("Calculating Stereotype V")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"V\"", "VB", "")

# Process: Select Formed, Sand
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 1 AND material = 4")
# Process: Calculate Stereotype X
arcpy.AddMessage("Calculating Stereotype X")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"X\"", "VB", "")

# Process: Select Minimally Formed, Natural
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 2 AND material = 3")
# Process: Calculate Stereotype Y
arcpy.AddMessage("Calculating Stereotype Y")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"Y\"", "VB", "")

# Process: Select Unformed, Natural
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "NEW_SELECTION", "assetgroup = 3 AND material = 3")
# Process: Calculate Stereotype Z
arcpy.AddMessage("Calculating Stereotype Z")
arcpy.CalculateField_management("Access_Stereotype", "stereotype", "\"Z\"", "VB", "")

#Clear Selection
arcpy.SelectLayerByAttribute_management("Access_Stereotype", "CLEAR_SELECTION")

#Add Field Add RD_Len
arcpy.AddMessage("Adding Field RD_Len")
arcpy.AddField_management("Access_Stereotype", "RD_Len", "DOUBLE", "", "", "", "", "NULLABLE", "NON_REQUIRED", "")

#Identity - splits road features intersecting the polygon boundaries of Protected Esate and Other Lands and supplies a field with a '-1' flag for all features that do not intersect estate
arcpy.AddMessage("Running Identity with Protected Areas and Other Lands")
arcpy.Identity_analysis(in_features="Access_Stereotype", identity_features="Protected_Areas_and_Other_Lands", out_feature_class="Access_1", join_attributes="ONLY_FID", cluster_tolerance="", relationship="NO_RELATIONSHIPS")

#Spatial Join - supplies field with NAMEABBREV and EST_TENURE for every record.
arcpy.AddMessage("Running Spatial Join with Protected Areas and Other Lands")
fieldmappings = arcpy.FieldMappings()
fieldmappings.addTable("Access_1")
fieldmappings.addTable("Protected_Areas_and_Other_Lands")
for field in fieldmappings.fields:
    if field.name in Estate_Field_List:
        fieldmappings.removeFieldMap(fieldmappings.findFieldMapIndex(field.name))

arcpy.SpatialJoin_analysis(target_features="Access_1", join_features="Protected_Areas_and_Other_Lands", out_feature_class="Access_2", join_operation="JOIN_ONE_TO_ONE", join_type="KEEP_ALL", field_mapping=fieldmappings, match_option="CLOSEST_GEODESIC", search_radius="", distance_field_name="")
fieldmappings.removeAll()

#Identity splits road features intesecting polygons of Work Unit Boundaries layer and supplies Region field for all features
arcpy.AddMessage("Running Identity with QPWS Work Unit Boundaries")
arcpy.Identity_analysis(in_features="Access_2", identity_features=WorkUnit_Boundaries, out_feature_class="Access_3", join_attributes="ALL", cluster_tolerance="", relationship="NO_RELATIONSHIPS")

# Calculate feature lengths based on UTM Zone
# Project into z54, select features in that zone and calculate lengths
arcpy.AddMessage("Calculating Feature Lengths")
arcpy.AddMessage("Zone 54")
arcpy.Project_management("Access_3", "Access_z54", MGA54)
arcpy.MakeFeatureLayer_management("Access_z54", "Access_z54_")
arcpy.MakeFeatureLayer_management(UTM_Zones, "UTM_Zones_")
arcpy.SelectLayerByAttribute_management("UTM_Zones_", "NEW_SELECTION", "Zone=54")
arcpy.SelectLayerByLocation_management("Access_z54_","HAVE_THEIR_CENTER_IN", UTM_Zones,"","NEW_SELECTION")
arcpy.CalculateField_management("Access_z54_", "RD_Len", "!shape.length@meters!", "PYTHON_9.3", "")
arcpy.SelectLayerByAttribute_management("Access_z54_", "CLEAR_SELECTION")
arcpy.SelectLayerByAttribute_management("UTM_Zones_", "CLEAR_SELECTION")

#Project into z55, select features in that zone and calculate lengths
arcpy.AddMessage("Calculating Feature Lengths")
arcpy.AddMessage("Zone 55")
arcpy.Project_management("Access_z54_", "Access_z55", MGA55)
arcpy.MakeFeatureLayer_management("Access_z55", "Access_z55_")
arcpy.SelectLayerByAttribute_management("UTM_Zones_", "NEW_SELECTION", "Zone=55")
arcpy.SelectLayerByLocation_management("Access_z55_","HAVE_THEIR_CENTER_IN", UTM_Zones,"","NEW_SELECTION")
arcpy.CalculateField_management("Access_z55_", "RD_Len", "!shape.length@meters!", "PYTHON_9.3", "")
arcpy.SelectLayerByAttribute_management("Access_z55_", "CLEAR_SELECTION")
arcpy.SelectLayerByAttribute_management("UTM_Zones_", "CLEAR_SELECTION")

#Project into z56, select features in that zone and calculate lengths
arcpy.AddMessage("Calculating Feature Lengths")
arcpy.AddMessage("Zone 56")
arcpy.Project_management("Access_z55_", "Access_z56", MGA56)
arcpy.MakeFeatureLayer_management("Access_z56", "Access_z56_")
arcpy.SelectLayerByAttribute_management("UTM_Zones_", "NEW_SELECTION", "Zone=55")
arcpy.SelectLayerByLocation_management("Access_z56_","HAVE_THEIR_CENTER_IN", UTM_Zones,"","NEW_SELECTION")
arcpy.CalculateField_management("Access_z56_", "RD_Len", "!shape.length@meters!", "PYTHON_9.3", "")
arcpy.SelectLayerByAttribute_management("Access_z56_", "CLEAR_SELECTION")
arcpy.SelectLayerByAttribute_management("UTM_Zones_", "CLEAR_SELECTION")

#Copy to final output file
arcpy.CopyFeatures_management("Access_z56_", "Access_Output")

# Process: Delete extra fields
arcpy.AddMessage("Deleting Fields from Table")
finalfields = arcpy.ListFields("Access_Output")
for field in finalfields:
    if field.name not in RVP_Field_List:
        arcpy.AddMessage(field.name)
        arcpy.DeleteField_management("Access_Output", field.name)

arcpy.AddMessage("Exporting Table to .csv")
arcpy.TableToTable_conversion("Access_Output", Out_Path, Out_Name)

# Process: Delete extra Feature Classes
arcpy.AddMessage("Deleting Intermediary Feature Classes")
arcpy.AddMessage("Access_1")
arcpy.Delete_management("Access_1")
arcpy.AddMessage("Access_2")
arcpy.Delete_management("Access_2")
arcpy.AddMessage("Access_3")
arcpy.Delete_management("Access_3")
arcpy.AddMessage("Access_z54")
arcpy.Delete_management("Access_z54")
arcpy.AddMessage("Access_z55")
arcpy.Delete_management("Access_z55")
arcpy.AddMessage("Access_z56")
arcpy.Delete_management("Access_z56")
arcpy.AddMessage("Protected_Areas_and_Other_Lands")
arcpy.Delete_management("Protected_Areas_and_Other_Lands")
arcpy.AddMessage("Access_Output")
# arcpy.Delete_management("Access_Output")



